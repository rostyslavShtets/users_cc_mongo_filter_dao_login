package dao

import (
	"log"

	// "users_cc_mongo_filter_dao"
	"users_cc_mongo_filter_dao_login/models"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"fmt"
)

type DAO struct {
	Server   string
	Database string
}

var db *mgo.Database

const (
	PEOPLE = "people"
	BOOKS = "books"
	USERS = "users"
)

// Establish a connection to database
func (p *DAO) Connect() {
	session, err := mgo.Dial(p.Server)
	if err != nil {
		log.Fatal(err)
	}
	db = session.DB(p.Database)
	fmt.Println("Database is connected!")

	// indexing collections
	IndexPeople(PEOPLE)
	IndexBooks(BOOKS)
}



// Find list of people
func (p *DAO) FindAllPeople() ([]models.Person, error) {
	var people []models.Person
	err := db.C(PEOPLE).Find(bson.M{}).All(&people)
	return people, err
}

// Find a person by its id
func (p *DAO) FindPersonById(id string) (*models.Person, error) {
	res := models.Person{}
	
	if err :=  db.C(PEOPLE).FindId(bson.ObjectIdHex(id)).One(&res); err != nil {
		return nil, err
	}

	return &res, nil
}

// Insert a person into database
func (p *DAO) InsertPerson(person *models.Person) error {
	return db.C(PEOPLE).Insert(&person)
}

// Delete an existing person
func (p *DAO) UpdatePerson(id string, person *models.Person) error {
	return db.C(PEOPLE).Update(bson.M{"_id": bson.ObjectIdHex(id)}, &person)
}

// Update an existing person
func (p *DAO) DeletePerson(id string) error {
	return db.C(PEOPLE).Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}

// filters
	// filter with one name
func (p *DAO) FilterOneName(name string) (*[]models.Person, error) {
	var peopleSlc []models.Person
	if err :=  db.C(PEOPLE).Find(bson.M{"firstname": name}).All(&peopleSlc); err != nil {
		return nil, err
	}
	return &peopleSlc, nil
}

	// filter with few names
func (p *DAO) FilterArrayNames(namesArray []string) (*[]models.Person, error) {
	var peopleSlc []models.Person
	if err :=  db.C(PEOPLE).Find(bson.M{"firstname": bson.M{"$in": namesArray}}).All(&peopleSlc); err != nil {
		return nil, err
	}
	return &peopleSlc, nil
}

	// filter with few names and age
func (p *DAO) FilterArrayNamesAge(namesArray []string, personAge int) (*[]models.Person, error) {
	var peopleSlc []models.Person
	if err :=  db.C(PEOPLE).Find(bson.M{"firstname": bson.M{"$in": namesArray}, "age": personAge}).All(&peopleSlc); err != nil {
		return nil, err
	}
	return &peopleSlc, nil
}

// filter with few names and age and skip and limit
func (p *DAO) FilterArrayNamesAgeSkipLimit(namesArray []string, personAge, peopleSkip, peopleLimit int) (*[]models.Person, error) {
	var peopleSlc []models.Person
	if err :=  db.C(PEOPLE).Find(bson.M{"firstname": bson.M{"$in": namesArray}}).Skip(peopleSkip).Limit(peopleLimit).All(&peopleSlc); err != nil {
		return nil, err
	}
	return &peopleSlc, nil
}

func (p *DAO) FilterObjectOneName(name string) (*[]models.Person, error) {
	var peopleSlc []models.Person
	if err :=  db.C(PEOPLE).Find(bson.M{"firstname": name}).All(&peopleSlc); err != nil {
		return nil, err
	}
	return &peopleSlc, nil
}

func (p *DAO) FilterObjectNameAndAge(name string, personAge int) (*[]models.Person, error) {
	var peopleSlc []models.Person
	if err :=  db.C(PEOPLE).Find(bson.M{"firstname": name, "age": personAge}).All(&peopleSlc); err != nil {
		return nil, err
	}
	return &peopleSlc, nil
}

// err := db.CollectionPeople().Find(bson.M{"firstname": filterStruct.FirstName}).All(&peopleSlc)



// dao methods for books
//  Find list of books
func (p *DAO) FindAllBooks() ([]models.Book, error) {
	var res []models.Book
	err := db.C(BOOKS).Find(bson.M{}).All(&res)
	return res, err
}

// Find a book by its id
func (p *DAO) FindBookById(id string) (*models.Book, error) {
	res := models.Book{}
	
	if err :=  db.C(BOOKS).FindId(bson.ObjectIdHex(id)).One(&res); err != nil {
		return nil, err
	}

	return &res, nil
}

// Insert a book into database
func (p *DAO) InsertBook(book *models.Book) error {
	return db.C(BOOKS).Insert(&book)
}

// Delete an existing book
func (p *DAO) UpdateBook(id string, book *models.Book) error {
	return db.C(BOOKS).Update(bson.M{"_id": bson.ObjectIdHex(id)}, &book)
}

// Update an existing book
func (p *DAO) DeleteBook(id string) error {
	return db.C(BOOKS).Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}


// dao methods for users
// Insert a user into database
func (p *DAO) InsertUser(user *models.User) error {
	return db.C(USERS).Insert(&user)
}

// Find all users
func (p *DAO) FindAllUsers() ([]models.User, error) {
	var res []models.User
	err := db.C(USERS).Find(bson.M{}).All(&res)
	return res, err
}

// Get user by email
func (p *DAO) FindUserByEmail(email string) (models.User, error) {
	var result models.User
	err := db.C(USERS).Find(bson.M{"email": email}).One(&result)
	return result, err
}

