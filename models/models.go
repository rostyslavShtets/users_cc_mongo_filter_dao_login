package models

import "gopkg.in/mgo.v2/bson"

type Person struct {
	Id bson.ObjectId	`json:"id" bson:"_id,omitempty"`
	UserID string		`json:"userID"`
	FirstName string	`json:"firstName"`
	LastName string		`json:"lastName"`
	Age int				`json:"age"`
	Password string		`json:"password"`
}

type Book struct {
	Id bson.ObjectId	`json:"id" bson:"_id,omitempty"`
	ISBN    string   	`json:"isbn"`
    Title   string   	`json:"title"`
    Author string 	 	`json:"author"`
    Price   int   	`json:"price"`
}

type Filter struct {
	Id bson.ObjectId		`json:"id" bson:"_id,omitempty"`
	UserID string			`json:"userID"`
	FirstName string		`json:"firstName"`
	LastName string			`json:"lastName"`
	Age int					`json:"age"`
	Limit int				`json:"limit"`
	Skip int				`json:"skip"`
}

type User struct {
	Id bson.ObjectId	`json:"id" bson:"_id,omitempty"`
	UserName string		`json:"userName"`
	Password string		`json:"password"`
	Email string		`json:"email"`
}