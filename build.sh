#!/usr/bin/env bash


set -x
set -e
docker-compose down
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o main

# docker build -t myapp_compose -f Dockerfile.scratch .
docker build -t myapp_cc_mongo_filter_dao_login .
docker-compose up

