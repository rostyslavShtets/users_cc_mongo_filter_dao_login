package middleware

import (
	"net/http"
	"users_cc_mongo_filter_dao_login/api"
	"fmt"
)
func AuthenticationMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		check := api.CheckIfUserIsAuthenticated(w, r)
		fmt.Println(check)
       	if check {
			next(w, r)
		} else {
			w.Write([]byte("<h1>you are not login<h1>"))
			return
		}
    })
}