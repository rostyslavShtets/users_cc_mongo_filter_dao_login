package api

import (
	"users_cc_mongo_filter_dao_login/models"
	"gopkg.in/mgo.v2/bson"
	"github.com/gorilla/mux"
	"net/http"
	"fmt"
	"log"
	"encoding/json"
	"io/ioutil"
)

// GET list of people
func AllPeopleEndPoint(w http.ResponseWriter, r *http.Request) {

	people, err := daoDB.FindAllPeople()
	if err != nil {
		log.Fatal(err)
		return
	}
	json.NewEncoder(w).Encode(people)
}

// GET a people by its ID
func FindPersonEndPoint(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["personID"]
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	
	rs, err := daoDB.FindPersonById(id)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}

	json.NewEncoder(w).Encode(rs)
}

// POST a new persom

func CreatePersonEndPoint(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	var person models.Person
	if err != nil {
		log.Println("handlers SaveIDID error:", err)
		http.Error(w, "can’t read body", http.StatusBadRequest)
		return
	}
	err = json.Unmarshal(body, &person)
	if err != nil {
			log.Println("handlers SaveID error:", err)
			http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
			return
		}
		fmt.Println(person)
			
	person.Id = bson.NewObjectId()

	if err = daoDB.InsertPerson(&person); err != nil {
		fmt.Println("error with insert")
		http.Error(w, http.StatusText(500), 500)
		return
	}

	fmt.Fprintf(w, "Person with ID %v created successfully\n", person.Id)
}

// PUT update an existing movie
func UpdatePersonEndPoint(w http.ResponseWriter, r *http.Request) {
	//get ID from request
	id := mux.Vars(r)["personID"]
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	fmt.Println("id - ", id)

	//read JSON from request body
	body, err := ioutil.ReadAll(r.Body)

	var person models.Person
	if err != nil {
		log.Println("handlers SaveIDID error:", err)
		http.Error(w, "can’t read body", http.StatusBadRequest)
		return
	}

	err = json.Unmarshal(body, &person)
	if err != nil {
		log.Println("handlers SaveID error:", err)
		http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
		return
	}

	if err = daoDB.UpdatePerson(id, &person); err != nil {
		fmt.Println("error with update")
		http.Error(w, http.StatusText(500), 500)
		return
	}
	fmt.Fprintf(w, "Person with ID = %v updated successfully\n", id)
}

// DELETE an existing person
func DeletePersonEndPoint(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["personID"]
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	fmt.Println("id:", id)
	
	if err := daoDB.DeletePerson(id); err != nil {
		http.Error(w, http.StatusText(500), 500)
		fmt.Println("can't delete:", err)
		return
	}
	fmt.Fprintf(w, "Person with ID = %v deleted successfully\n", id)
}