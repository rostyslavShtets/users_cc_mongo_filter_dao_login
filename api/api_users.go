package api

import (
	"users_cc_mongo_filter_dao_login/models"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"fmt"
	"log"
	"encoding/json"
	"io/ioutil"
)

// AllUsersEndPoint GET list of users
func AllUsersEndPoint(w http.ResponseWriter, r *http.Request) {
	users, err := daoDB.FindAllUsers()
	if err != nil {
		log.Fatal(err)
		return
	}
	json.NewEncoder(w).Encode(users)
}

// CreateUserEndPoint POST a new user
func CreateUserEndPoint(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	var user models.User
	if err != nil {
		log.Println("handlers SaveIDID error:", err)
		http.Error(w, "can’t read body", http.StatusBadRequest)
		return
	}
	err = json.Unmarshal(body, &user)
	if err != nil {
			log.Println("handlers SaveID error:", err)
			http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
			return
		}
	fmt.Println(user)
			
	user.Id = bson.NewObjectId()

	if err = daoDB.InsertUser(&user); err != nil {
		fmt.Println("error with insert")
		http.Error(w, http.StatusText(500), 500)
		return
	}

	fmt.Fprintf(w, "Person with ID %v created successfully\n", user.Id)
}

//FindUserByEmailEndPointGET find user by email - GET request
func FindUserByEmailEndPointGET(w http.ResponseWriter, r *http.Request) {
	email := mux.Vars(r)["email"]
	if email == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	
	rs, err := daoDB.FindUserByEmail(email)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}

	json.NewEncoder(w).Encode(rs)
}

// FindUserByEmailEndPointPOST find user by email - POST request
func FindUserByEmailEndPointPOST(email string) (models.User, error) {
	rs, err := daoDB.FindUserByEmail(email)
	if err != nil {
		return rs, err
	}
	return rs, err
}