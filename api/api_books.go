package api

import (
	"users_cc_mongo_filter_dao_login/models"
	"gopkg.in/mgo.v2/bson"
	"github.com/gorilla/mux"
	"net/http"
	"fmt"
	"log"
	"encoding/json"
	"io/ioutil"
)

// books API
// GET list of books
func AllBooksEndPoint(w http.ResponseWriter, r *http.Request) {

	people, err := daoDB.FindAllBooks()
	if err != nil {
		log.Fatal(err)
		return
	}
	json.NewEncoder(w).Encode(people)
}

// GET a book by its ID
func FindBookEndPoint(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["bookID"]
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	
	rs, err := daoDB.FindBookById(id)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}

	json.NewEncoder(w).Encode(rs)
}

// POST a new book
func CreateBookEndPoint(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	var book models.Book
	if err != nil {
		log.Println("handlers SaveIDID error:", err)
		http.Error(w, "can’t read body", http.StatusBadRequest)
		return
	}
	err = json.Unmarshal(body, &book)
	if err != nil {
			log.Println("handlers SaveID error:", err)
			http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
			return
		}
		fmt.Println(book)
			
	book.Id = bson.NewObjectId()

	if err = daoDB.InsertBook(&book); err != nil {
		fmt.Println("error with insert")
		http.Error(w, http.StatusText(500), 500)
		return
	}

	fmt.Fprintf(w, "Person with ID %v created successfully\n", book.Id)
}

// PUT update an existing book
func UpdateBookEndPoint(w http.ResponseWriter, r *http.Request) {
	//get ID from request
	id := mux.Vars(r)["bookID"]
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	fmt.Println("id - ", id)

	//read JSON from request body
	body, err := ioutil.ReadAll(r.Body)

	var book models.Book
	if err != nil {
		log.Println("handlers SaveIDID error:", err)
		http.Error(w, "can’t read body", http.StatusBadRequest)
		return
	}

	err = json.Unmarshal(body, &book)
	if err != nil {
		log.Println("handlers SaveID error:", err)
		http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
		return
	}

	if err = daoDB.UpdateBook(id, &book); err != nil {
		fmt.Println("error with update")
		http.Error(w, http.StatusText(500), 500)
		return
	}
	fmt.Fprintf(w, "Person with ID = %v updated successfully\n", id)
}

// DELETE an existing book
func DeleteBookEndPoint(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["bookID"]
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	fmt.Println("id:", id)
	
	if err := daoDB.DeleteBook(id); err != nil {
		http.Error(w, http.StatusText(500), 500)
		fmt.Println("can't delete:", err)
		return
	}
	fmt.Fprintf(w, "Book with ID = %v deleted successfully\n", id)
}