package api

import (
	"net/http"
	"log"
	"encoding/json"
	"strconv"
	"fmt"
)

//func for filter
func GetPeopleFilter(w http.ResponseWriter, r *http.Request) { 

	// get map from query
	notice := "Query string is:" + r.Method + r.URL.String()
	log.Println(notice)
	valuesFromQuery := r.URL.Query()
	fmt.Println("values:", valuesFromQuery);
	fmt.Println("map names:", valuesFromQuery["name"]);
	fmt.Println("limit:", valuesFromQuery.Get("limit"));
	
	peopleSkipString := valuesFromQuery.Get("skip")
	peopleSkipInt, _ := strconv.Atoi(peopleSkipString)
	peopleLimitString := valuesFromQuery.Get("limit")
	peopleLimitInt, _ := strconv.Atoi(peopleLimitString)
	personAgeString := r.URL.Query().Get("age")
	personAgeInt, _ := strconv.Atoi(personAgeString)
	fmt.Println("age:", personAgeString);
	
	
	//working with name
	if valuesFromQuery.Get("name") != "" {
		namesArray := valuesFromQuery["name"]
		fmt.Println("GET params were:", namesArray);
		fmt.Println("array:", namesArray);
		

		if len(namesArray) == 1 {
			peopleSlc, err := daoDB.FilterOneName(namesArray[0])
			if err != nil {
				http.Error(w, http.StatusText(400), 400)
				return
			}
			json.NewEncoder(w).Encode(peopleSlc)
			return
		}

		//if we have only more then one name
		if len(namesArray) > 1 && personAgeString == "" && valuesFromQuery.Get("limit") == ""{
			fmt.Println("namesArray")
			peopleSlc, err := daoDB.FilterArrayNames(namesArray)
			if err != nil {
				http.Error(w, http.StatusText(400), 400)
				return
			}
			json.NewEncoder(w).Encode(peopleSlc)
			return
		}
		
		//if we have only more then one name and filter by age
		if len(namesArray) > 1 && personAgeString != "" && valuesFromQuery.Get("limit") == ""{
			fmt.Println("namesArray and age")
			peopleSlc, err := daoDB.FilterArrayNamesAge(namesArray, personAgeInt)
			if err != nil {
				http.Error(w, http.StatusText(400), 400)
				return
			}
			json.NewEncoder(w).Encode(peopleSlc)
			return
		}

		//if we have only more then one name and limit it and skip
		if len(namesArray) > 1 && personAgeString == "" && valuesFromQuery.Get("limit") != "" && valuesFromQuery.Get("skip") != "" {
			fmt.Println("---if we have only more then one name and limit it---")
			fmt.Println("namesArray anf age and skip and limit")
			peopleSlc, err := daoDB.FilterArrayNamesAgeSkipLimit(namesArray, personAgeInt, peopleSkipInt, peopleLimitInt)
			if err != nil {
				http.Error(w, http.StatusText(400), 400)
				return
			}
			json.NewEncoder(w).Encode(peopleSlc)
			return
		}

	}
	
}

