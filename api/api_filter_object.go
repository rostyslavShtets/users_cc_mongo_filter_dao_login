package api

import (
	"net/http"
	"log"
	"encoding/json"
	"fmt"
	"net/url"
	"users_cc_mongo_filter_dao_login/models"
)

// to set filter like object
func GetPeopleFilterObject(w http.ResponseWriter, r *http.Request) {
	//create istance 
	filterStruct := models.Filter{}
	//handle filter query
	filterFromUrl := r.URL.Query().Get("filter")
	filterJson, err:= url.QueryUnescape(filterFromUrl)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	fmt.Println("values:", filterJson);
	json.Unmarshal([]byte(filterJson), &filterStruct)
	fmt.Println("values:", filterStruct.FirstName);
	
	// get map from query
	notice := "Query string is:" + r.Method + r.URL.String()
	log.Println(notice)
	
	//working with name
	if filterStruct.FirstName != "" && filterStruct.Age == 0 {
		// var peopleSlc []models.Person
		// err := db.CollectionPeople().Find(bson.M{"firstname": filterStruct.FirstName}).All(&peopleSlc)
		peopleSlc, err := daoDB.FilterObjectOneName(filterStruct.FirstName)
		if err != nil {
			http.Error(w, http.StatusText(400), 400)
			return
		}
		json.NewEncoder(w).Encode(peopleSlc)
		return
	}
		
	//working with name and age
	if filterStruct.FirstName != "" && filterStruct.Age != 0 {
		fmt.Println("working object with name and age")
		peopleSlc, err := daoDB.FilterObjectNameAndAge(filterStruct.FirstName, filterStruct.Age)
		if err != nil {
			http.Error(w, http.StatusText(400), 400)
			return
		}
		json.NewEncoder(w).Encode(peopleSlc)
		return
	}
		
}