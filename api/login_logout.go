package api

import (
	"fmt"
	"net/http"
	"github.com/gorilla/sessions"
	// "users_cc_mongo_filter_dao_login/models"
	// "net/http"
	"log"
	// "encoding/json"
	// "io/ioutil"
)


var (
	key = []byte("super-secret-key")
	store = sessions.NewCookieStore(key)
)

// func init() {
// 	store.Options = &sessions.Options{
// 		Path:     "/",
// 		MaxAge:   86400 * 7,
// 		HttpOnly: true,
// 	}
// }

func CheckIfUserIsAuthenticated(w http.ResponseWriter, r *http.Request) bool {
	session, _ := store.Get(r, "cookie-name")
	
	// Check if user is authenticated
	if auth, ok := session.Values["authenticated"].(bool); !auth || !ok {
		fmt.Println(auth, ok)
		return false
	}
	return true
}

func LoginPage(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.ServeFile(w, r, "templates/login.html")
		return
	}

	session, _ := store.Get(r, "cookie-name")

	//get user from put request
	userEmail := r.FormValue("userEmail")
	userPassword := r.FormValue("password")
	fmt.Println(userEmail)
	//get request to database
	userFromDB, err := FindUserByEmailEndPointPOST(userEmail)
	if err != nil {
		log.Println("can’t find user:", err)
		http.Redirect(w, r, "/login", 301)
		return
	}
	fmt.Println(userFromDB)
	if (userEmail == userFromDB.Email && userPassword == userFromDB.Password) {
		// Set user as authenticated
		session.Values["authenticated"] = true
		// session.Save(r, w)
		err := session.Save(r, w)

		if err != nil {
			log.Println(err)
			return
		}
		log.Println("You just have login")
		fmt.Fprintf(w, "You just have login %v. Thanks\n", userFromDB.UserName)
		w.Write([]byte("Hello " + userFromDB.UserName + "!"))
		
	} else {
		log.Println("password or user isn't correct", err)
		http.Error(w, "password or user isn't correct", http.StatusBadRequest)
		w.Write([]byte("password or user isn't correct"))
	}
}

func Logout(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")

	// Revoke users authentication
	session.Values["authenticated"] = false
	session.Save(r, w)
}

func Secret(w http.ResponseWriter, r *http.Request) {
	fmt.Println("secret")
	session, _ := store.Get(r, "cookie-name")

	// Check if user is authenticated
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		http.Error(w, "Forbidden", http.StatusForbidden)
		return
	}

	// Print secret message
	fmt.Fprintln(w, "The cake is a lie!")
}

func LoginTest(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")

	// Authentication goes here
	// ...

	// Set user as authenticated
	session.Values["authenticated"] = true
	session.Save(r, w)
}