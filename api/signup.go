package api

import (
	"net/http"
	"users_cc_mongo_filter_dao_login/models"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	// "net/http"
	// "fmt"
	// "log"
	// "encoding/json"
	// "io/ioutil"
)

func SignUpPage(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.ServeFile(w, r, "templates/signup.html")
		return
	}

	userName := r.FormValue("userName")
	userEmail := r.FormValue("email")
	password := r.FormValue("password")
	
	var user models.User
	user.UserName = userName
	user.Email = userEmail
	user.Password = password
	
	fmt.Println(user)
			
	user.Id = bson.NewObjectId()

	if err := daoDB.InsertUser(&user); err != nil {
		fmt.Println("error with insert")
		http.Error(w, http.StatusText(500), 500)
		return
	}

	fmt.Fprintf(w, "Person with ID %v created successfully\n", user.Id)
}