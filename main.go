package main

import (
	"users_cc_mongo_filter_dao_login/api"
	"users_cc_mongo_filter_dao_login/utils"
	"users_cc_mongo_filter_dao_login/middleware"
	"github.com/gorilla/mux"
	// "github.com/gorilla/sessions"
	"fmt"
	"log"
	"net/http"
)
const port = ":8081"

// func homeHandler(rw http.ResponseWriter, r *http.Request) {
// 	fmt.Println("Home")
// 	}

func main() {
	// additional internal check 
	utils.Hello()
	fmt.Println("new_new")

	// init DB
	api.InitDB()
			
	r := mux.NewRouter()
	// r.Path("/").HandlerFunc(homeHandler)


	r.HandleFunc("/", api.HomePage)

	//work with session example
	r.HandleFunc("/secret", api.Secret).Methods("GET")
	r.HandleFunc("/loginTest", api.LoginTest)
	

	r.HandleFunc("/login", api.LoginPage)
	r.HandleFunc("/logout", api.Logout).Methods("GET")
	r.HandleFunc("/signup", api.SignUpPage)
	
		

	//request for people
	r.HandleFunc("/people", middleware.AuthenticationMiddleware(api.AllPeopleEndPoint)).Methods("GET")
	r.HandleFunc("/people/{personID}", middleware.AuthenticationMiddleware(api.FindPersonEndPoint)).Methods("GET")
	r.HandleFunc("/filter/people", api.GetPeopleFilter).Methods("GET")
	r.HandleFunc("/filter_object/people", api.GetPeopleFilterObject).Methods("GET")
	r.HandleFunc("/people", middleware.AuthenticationMiddleware(api.CreatePersonEndPoint)).Methods("POST")
	r.HandleFunc("/people/{personID}", middleware.AuthenticationMiddleware(api.UpdatePersonEndPoint)).Methods("PUT")
	r.HandleFunc("/people/{personID}", middleware.AuthenticationMiddleware(api.DeletePersonEndPoint)).Methods("DELETE")
	
	//request for books
	r.HandleFunc("/books", middleware.AuthenticationMiddleware(api.AllBooksEndPoint)).Methods("GET")
	r.HandleFunc("/books/{bookID}", api.FindBookEndPoint).Methods("GET")
	r.HandleFunc("/books", api.CreateBookEndPoint).Methods("POST")
	r.HandleFunc("/books/{bookID}", api.UpdateBookEndPoint).Methods("DELETE")
	r.HandleFunc("/books/{bookID}", api.DeleteBookEndPoint).Methods("PUT")

	//request for users
	r.HandleFunc("/users", api.AllUsersEndPoint).Methods("GET")



	fmt.Printf("Serv on port %v....\n", port)
	if err := http.ListenAndServe(port, r); err != nil {
		log.Fatal(err)
	}
	// log.Fatal(http.ListenAndServe(port, r))
	// r.HandleFunc("/registration", api.CreateUserEndPoint).Methods("POST")
		// r.HandleFunc("/users/email/{email}", api.FindUserByEmailEndPointGET).Methods("GET")
}


